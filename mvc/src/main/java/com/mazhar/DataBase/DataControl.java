/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.DataBase;

import com.mazhar.Model.CourseModel;
import com.mazhar.Model.StudentModel;
import com.mazhar.Model.TeacherModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author pk
 */
public class DataControl implements DataBaseModel{
    ArrayList<String> queries=new ArrayList<>();
    ArrayList<String> queriesdelete=new ArrayList<>();
     ArrayList<String> queriesupdate=new ArrayList<>();
     private ResultSet rs;
    private PreparedStatement pst;
    @Override
    public boolean addTeacher(String id, String Name, String cname) {
         try{
             Connection con=DBConnection.getConnectorManager().getDBConnecton();
             String sql= "INSERT INTO `teacher`(`TeacherID`, `TeacherName`, `TeacherType`) VALUES (?,?,?)";
             pst= con.prepareStatement(sql);
             pst.setString(1,id );
             pst.setString(2,Name);
             pst.setString(3, cname);
             pst.executeUpdate();
           return true;
            
       }
       catch(Exception e){
           return false;
       }    
    }

    
    
    public void addQueriesStudent(String id, String Name, String Classs, String selectedcourse){
        String sql= "INSERT INTO student(studentID, StudentName, Class, CourseName) VALUES ('"+id+"','"+Name+"','"+Classs+"','"+selectedcourse+"')";
        queries.add(sql);
        
    }
    public void UpdateQueriesStudent(int id, String Name, String Classs, String selectedcourse){
        String sql= "UPDATE `student` SET StudentName='"+Name+"',`Class`='"+Classs+"',`CourseName`='"+selectedcourse+"' WHERE id='"+id+"'";
        queriesupdate.add(sql);
        
    }
    @Override
    public boolean ExecuteQueryStudentBatch() {
        try {
            Connection con=DBConnection.getConnectorManager().getDBConnecton();
            Statement stmt= con.createStatement();
            for(String query: queries)
            {  
                stmt.addBatch(query);
            }
            stmt.executeBatch();
            return true;
        } catch (SQLException ex) {
            //Logger.getLogger(DataControl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    public void addDeleteQueries(int ID){
        String sql= "DELETE FROM `student` WHERE id='"+ID+"'";
        queriesdelete.add(sql);
    }
    @Override
    public boolean DeleteStudentBatch() {
       try {
            Connection con=DBConnection.getConnectorManager().getDBConnecton();
            Statement stmt= con.createStatement();
            for(String query: queriesdelete)
            {  
                stmt.addBatch(query);
            }
            stmt.executeBatch();
            return true;
        } catch (SQLException ex) {
          
            return false;
        }
    }

    @Override
    public boolean UpdateStudentBatch() {
        try {
            Connection con=DBConnection.getConnectorManager().getDBConnecton();
            Statement stmt= con.createStatement();
            for(String query: queriesupdate)
            {  
                stmt.addBatch(query);
            }
            stmt.executeBatch();
            return true;
        } catch (SQLException ex) {
           
            return false;
        }
    }

    @Override
    public List<StudentModel> getAllStudent() {
        ArrayList<StudentModel> studentmodel= new ArrayList<>();
         try{
            Connection con=DBConnection.getConnectorManager().getDBConnecton();
             String query = "SELECT * FROM student";
            pst= con.prepareStatement(query);
            rs=pst.executeQuery();
            while (rs.next()) {
                int index= rs.getInt("id");
                String id= rs.getString("studentID");
                String name = rs.getString("StudentName");
                String price = rs.getString("class");
                String SelectedCourse= rs.getString("CourseName");
                studentmodel.add(new StudentModel(id,name,price,SelectedCourse, index));
            }
             return studentmodel;
        } catch (Exception e) {
            return null;
        }
        
    }

    @Override
    public List<TeacherModel> getAllTeacher() {
       ArrayList<TeacherModel> teachermodel= new ArrayList<>();
         try{
            Connection con=DBConnection.getConnectorManager().getDBConnecton();
            String query = "SELECT * FROM teacher`";
            pst= con.prepareStatement(query);
            rs=pst.executeQuery();
            while (rs.next()) {
                String id= rs.getString("TeacherID");
                String name = rs.getString("TeacherName");
                String type = rs.getString("TeacherType");
                teachermodel.add(new TeacherModel(id,name,type));
            }
             return teachermodel;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<CourseModel> getCourseDescription() {
        ArrayList<CourseModel> coursemodel= new ArrayList<>();
         try{
            Connection con=DBConnection.getConnectorManager().getDBConnecton();
            String query = "SELECT CourseName, CourseFee, teacher.TeacherName as tname from coursestructure left join teacher on coursestructure.CourseName=teacher.TeacherType";
            pst= con.prepareStatement(query);
            rs=pst.executeQuery();
            while (rs.next()) {
                String id= rs.getString("CourseName");
                String name = rs.getString("CourseFee");
                String type = rs.getString("tname");
                coursemodel.add(new CourseModel(id,name,type));
            }
             return coursemodel;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean updateTeacher() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteTeacher() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public void addMultiStudent()
    {
        
    }
    
}
