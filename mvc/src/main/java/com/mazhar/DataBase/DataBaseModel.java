/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.DataBase;

import com.mazhar.Model.CourseModel;
import com.mazhar.Model.StudentModel;
import com.mazhar.Model.TeacherModel;
import java.util.List;

/**
 *
 * @author pk
 */
public interface DataBaseModel {
    public boolean addTeacher(String id, String Name, String cname);
    public boolean ExecuteQueryStudentBatch();
    public boolean DeleteStudentBatch();
    public boolean UpdateStudentBatch();

    /**
     *
     * @return
     */
    public List<StudentModel> getAllStudent();
    public List<TeacherModel> getAllTeacher();
    public List<CourseModel> getCourseDescription();
    public boolean updateTeacher();
    public boolean deleteTeacher();
}
