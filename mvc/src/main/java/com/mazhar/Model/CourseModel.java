/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.Model;

/**
 *
 * @author pk
 */
public class CourseModel {
    private String Name;
    private String fee;
    private String CourseTeacher;

    public CourseModel(String Name, String fee, String CourseTeacher) {
        this.Name = Name;
        this.fee = fee;
        this.CourseTeacher = CourseTeacher;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getCourseTeacher() {
        return CourseTeacher;
    }

    public void setCourseTeacher(String CourseTeacher) {
        this.CourseTeacher = CourseTeacher;
    }
    
    
}
