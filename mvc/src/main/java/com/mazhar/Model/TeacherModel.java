/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.Model;

/**
 *
 * @author pk
 */
public class TeacherModel {
    private String TeacherId;
    private String TeacherName;
    private String CourseName;

    public TeacherModel(String TeacherId, String TeacherName, String CourseName) {
        this.TeacherId = TeacherId;
        this.TeacherName = TeacherName;
        this.CourseName = CourseName;
    }

    public String getTeacherId() {
        return TeacherId;
    }

    public void setTeacherId(String TeacherId) {
        this.TeacherId = TeacherId;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public void setTeacherName(String TeacherName) {
        this.TeacherName = TeacherName;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String CourseName) {
        this.CourseName = CourseName;
    }
    
    
}
