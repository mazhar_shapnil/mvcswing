/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.Model;

/**
 *
 * @author mazhar
 */
public class StudentModel {
    private int index;
    private String Id;
    private String name;
    private String classs;
    private String courses;

    public StudentModel(String Id, String name, String classs, String courses, int index) {
        this.Id = Id;
        this.name = name;
        this.classs = classs;
        this.courses = courses;
        this.index= index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClasss() {
        return classs;
    }

    public void setClasss(String classs) {
        this.classs = classs;
    }

    public String getCourses() {
        return courses;
    }

    public void setCourses(String courses) {
        this.courses = courses;
    }
    
}
