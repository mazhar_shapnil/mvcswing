/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.Controller;


import com.mazhar.DataBase.DataControl;
import com.mazhar.Model.CourseModel;
import com.mazhar.Model.StudentModel;
import com.mazhar.Model.TeacherModel;
import com.mazhar.View.AddMultiStudent;
import com.mazhar.View.AddStudent;
import com.mazhar.View.AddTeacher;
import com.mazhar.View.Dashboard;
import com.mazhar.View.UpdateStudent;
import com.mazhar.View.deleteStudent;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author mazhar
 */
public class DashboardController {
     private Dashboard dashboard;
     private AddTeacher addteacher = new AddTeacher();
     private AddStudent addstudent = new AddStudent();
     private AddMultiStudent multistudent= new AddMultiStudent();
     private deleteStudent deletestudent= new deleteStudent();
     private UpdateStudent updatestudent= new UpdateStudent();
     private UpdateStudentController updatestucontroller;
     private DeleteStudentController deletecontroller;
     private AddMultiStudentController multicontroller;
     private StudentModel student;
     private DataControl datacontrol = new DataControl();
     AddStudentController addstudentcontroller;
    public DashboardController(Dashboard dashboard) {
        this.dashboard = dashboard;
       
       
    }
     
    public void initDashboard(){
        dashboard.setVisible(true);
        showdataStudent();
       
    } 
    public void DeleteTec(){
        
    }
    public void UpdateTec(){
        
    }
    public void deleteBatch(){

      deletecontroller= new DeleteStudentController(deletestudent);
      deletecontroller.initDeleteStudentController();
    }
    public void UpdateBatch()
    {
        
         updatestucontroller= new UpdateStudentController(updatestudent);
         updatestucontroller.initUpdateStudentController();
    }
    public void addStudent(){
        dashboard.dispose();
        
        addstudentcontroller= new AddStudentController(addstudent);
        addstudentcontroller.initAddStudent();
    }
    
    public void ShowStudents()
    {
         showdataStudent();
        
    }
    public void addTeacher()
    {
        dashboard.dispose();
        AddTeacherController addTecControl= new AddTeacherController(addteacher);
        addTecControl.initTeacher();
    }
    public void ShowTeachers()
    {
         showdataTeacher();
    }
    
    public void ShowCourses(){
         showdataCourse();
    }
    public void showdataStudent(){
         DefaultTableModel model = new DefaultTableModel(new String[] {"index","Student Id","Name", "Class", "Selected Courses"},0);
            for(StudentModel student: datacontrol.getAllStudent())
            {
                int i=student.getIndex();
                String a= student.getId();
                String b= student.getName();
                String c= student.getClasss();
                String d= student.getCourses();
                model.addRow(new Object[]{i,a,b,c,d}); 
            }
            dashboard.getDataTable().setModel(model);
            dashboard.getHearder().setText("Student Info");
            dashboard.getUpdateBatchBtn().setVisible(true);
            dashboard.getBatchDeleteBtn().setVisible(true);
            dashboard.getTecDeleteBtn().setVisible(false);
            dashboard.getTecUpdateBtn().setVisible(false);
       
    }
    public void showdataTeacher(){
         DefaultTableModel model = new DefaultTableModel(new String[] {"Teacher Id","Name", "Course Name"},0);
            for(TeacherModel teacher: datacontrol.getAllTeacher())
            {
                String a= teacher.getTeacherId();
                String b= teacher.getTeacherName();
                String c= teacher.getCourseName();
               
                model.addRow(new Object[]{a,b,c}); 
            }
            dashboard.getDataTable().setModel(model);
            dashboard.getHearder().setText("Teacher Info");
            dashboard.getUpdateBatchBtn().setVisible(false);
            dashboard.getBatchDeleteBtn().setVisible(false);
            dashboard.getTecDeleteBtn().setVisible(true);
            dashboard.getTecUpdateBtn().setVisible(true);
        
    }
    public void showdataCourse()
    {
         DefaultTableModel model = new DefaultTableModel(new String[] {"Course Name","Fee", "Course Teacher"},0);
          for(CourseModel course: datacontrol.getCourseDescription())
            {
                String a= course.getName();
                String b= course.getFee();
                String c= course.getCourseTeacher();
               
                model.addRow(new Object[]{a,b,c}); 
            }
         dashboard.getDataTable().setModel(model);
         dashboard.getHearder().setText("Course Info");
         dashboard.getUpdateBatchBtn().setVisible(false);
         dashboard.getBatchDeleteBtn().setVisible(false);
         dashboard.getTecDeleteBtn().setVisible(false);
         dashboard.getTecUpdateBtn().setVisible(false);
    }
    private void datadisplayMouseClicked(java.awt.event.MouseEvent evt) {
    TableModel model = dashboard.getDataTable().getModel();
       int index = dashboard.getDataTable().getSelectedRow();
      
        String id= model.getValueAt(index,0).toString();
        String name= model.getValueAt(index,1).toString();
        String price= model.getValueAt(index, 2).toString();
        if(dashboard.getHearder().getText().equalsIgnoreCase("Student Info"))
         {
                    String qnty= model.getValueAt(index, 3).toString();
                    updatestudent.getSeletedcourseField().setText(qnty);
         }
        updatestudent.getUpdateIDField().setText(id);
        updatestudent.getSNmUpdateBtn().setText(name);
        updatestudent.getSClaUpdateBtn().setText(price);
        deletestudent.getDeleteIDField().setText(id);
        //String v= deletestudent.getDeleteIDField().getText();
       // JOptionPane.showMessageDialog(null, v);
        
    }
    public void loadMultiStudentForm()
    {
        multicontroller= new AddMultiStudentController(multistudent);
        multicontroller.initMulti();
        
    }
    
    public  void dataTableClicked(java.awt.event.MouseEvent evt) {
                TableModel model = dashboard.getDataTable().getModel();
                int index = dashboard.getDataTable().getSelectedRow();
                String id= model.getValueAt(index,0).toString();
                datadisplayMouseClicked(evt);
    }
}
