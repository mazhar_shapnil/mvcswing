/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.Controller;

import com.mazhar.DataBase.DBConnection;
import com.mazhar.DataBase.DataControl;
import com.mazhar.Model.CourseDescription;
import com.mazhar.Model.StudentModel;
import com.mazhar.View.AddStudent;
import com.mazhar.View.Dashboard;

import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author pk
 */
public class AddStudentController {
    private AddStudent addstudent ;
    private StudentModel studentmodel;
    private PreparedStatement pst;
    public int counter=0;
    public int itemSelectionIndex=0;
    String value= null;
    String data="";
    DataControl datacontrol= new DataControl();
    
   
    ArrayList<String> SelectedCourse=new ArrayList<>();
    public AddStudentController(AddStudent addstudent) {
        this.addstudent = addstudent;
        addstudent.setVisible(true);
    }
    
    public void initAddStudent() {
       // addstudent.getStu_saveBtn().addActionListener(e -> saveStudent());
        
        ShowCourses();
         value = JOptionPane.showInputDialog(null, "How Many Student do you want to add ?");
         if(value.equals(""))
        {
            value= "-1";
        }
         if(Integer.parseInt(value)<=0)
         {
              JOptionPane.showMessageDialog(null, "value must be gather than 0");
              BackOption();
         }
         else
         {
              addstudent.setVisible(true);
         
         addstudent.getStu_saveBtn().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               
                counter++;
                saveStudent();
                if(counter==Integer.parseInt(value))
                {
                     
                         if(datacontrol.ExecuteQueryStudentBatch()){
                            JOptionPane.showMessageDialog(null, (counter)+" items added successfully");
                            addstudent.dispose();
                            DashboardController dcontrol= new DashboardController(new Dashboard());
                            dcontrol.initDashboard();
                         }
                         else
                         {
                             JOptionPane.showMessageDialog(null, "error in adding");
                         }
                         
                        
                       
                }
               
            }
        });
        addstudent.getBackBtn().addActionListener(e -> BackOption());
        addstudent.getCourseList().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                itemSelectionIndex= addstudent.getCourseList().getSelectedIndex();
                switch(itemSelectionIndex)
                {
                    case 0:
                         SelectedCourse.add("BANGLA");
                         break;
                    case 1: 
                         SelectedCourse.add("ENGLISH");
                         break;
                    case 2: 
                         SelectedCourse.add("PHYSICS");
                         break;
                    case 3:
                         SelectedCourse.add("MATHEMATICS");
                         break;
                    case 4:
                         SelectedCourse.add("CHERMISTRY");
                         break;
                    case 5:
                         SelectedCourse.add("BIOLOGY");
                         break;
                         
                }
                for(String s:SelectedCourse )
                {
                   data+= s+",";
                }
                addstudent.getSelectedSubjectList().setText(data);
                data="";
            }
        });
         }
         
    }
    
    public void saveStudent()
    { 
     
       String id=addstudent.getS_idField().getText() ;
       String name=addstudent.getS_nameField().getText();
       String classs= addstudent.getS_classField().getText();
       //String selectedcourse = addstudent.getCourseList().getItemAt(itemSelectionIndex);
       for (String selectedcourse :SelectedCourse )
       {
       datacontrol.addQueriesStudent(id, name, classs,selectedcourse);
       }
       SelectedCourse.clear();
       data="";
       JOptionPane.showMessageDialog(null, "counter="+counter+"  loop range:"+value); 
       resets();
    }

    public void BackOption(){
        addstudent.dispose();
        DashboardController dcontrol= new DashboardController(new Dashboard());
        dcontrol.initDashboard();
    }
    public void resets(){
       addstudent.getS_idField().setText("");
       addstudent.getS_nameField().setText("");
       addstudent.getS_classField().setText("");
       addstudent.getSelectedSubjectList().setText("");
    }
    
    public void ShowCourses(){
        for(CourseDescription.CourseName cname:CourseDescription.CourseName.values() )
            {
                addstudent.getCourseList().addItem(cname.name());
            }
       
    }
}
