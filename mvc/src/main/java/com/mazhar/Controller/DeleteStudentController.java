/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.Controller;

import com.mazhar.DataBase.DataControl;
import com.mazhar.View.Dashboard;
import com.mazhar.View.deleteStudent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author pk
 */
public class DeleteStudentController {
    private deleteStudent deletestudent;
    private String value= "";
    private int counter= 0;
    private DataControl datacontrol = new DataControl();
    public DeleteStudentController(deleteStudent deletestudent) {
        this.deletestudent = deletestudent;
    }
    
    public void initDeleteStudentController(){
        value = JOptionPane.showInputDialog(null, "How Many Student do you want to delete ?");
        if(value.equals(""))
        {
            value= "-1";
        }
        if(Integer.parseInt(value)<=0)
        {
             JOptionPane.showMessageDialog(null, "value must be gather than 0");
             deletestudent.setVisible(false);
        }
        else
        {
            
        deletestudent.setVisible(true);
        deletestudent.getDeleteokBtn().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               
                counter++;
                StudentDelete();
                if(counter==Integer.parseInt(value))
                {
                     
                         if(datacontrol.DeleteStudentBatch()){
                            JOptionPane.showMessageDialog(null, (counter)+" items added successfully");
                            deletestudent.dispose();
                            DashboardController dcontrol= new DashboardController(new Dashboard());
                            dcontrol.initDashboard();
                         }
                         else
                         {
                             JOptionPane.showMessageDialog(null, "error in Delete");
                         }
                         
                        
                       
                }
               
            }
        });
         }
        
    }
    public void StudentDelete(){
        int id= Integer.parseInt(deletestudent.deleteIDField.getText());
        datacontrol.addDeleteQueries(id);
        JOptionPane.showMessageDialog(null, "One student added to the stack");
        resets();
    }
    
     public void resets(){
       deletestudent.deleteIDField.setText("");
    }
}
