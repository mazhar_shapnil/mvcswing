/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.Controller;

import com.mazhar.DataBase.DataControl;
import com.mazhar.Model.CourseDescription;
import com.mazhar.View.Dashboard;
import com.mazhar.View.UpdateStudent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author pk
 */
public class UpdateStudentController {
    private UpdateStudent updatestudent;
    private DataControl datacontrol = new DataControl();
     public int itemSelectionIndex=0;
     public String SelectedCourse= "";
     public String value="";
     public int counter =0;
    public UpdateStudentController(UpdateStudent updatestudent) {
        this.updatestudent = updatestudent;
    }
    
    public void initUpdateStudentController()
    {
            
            value = JOptionPane.showInputDialog(null, "How Many Student do you want to Update ?");
            if(value.equals(""))
             {
            value= "-1";
             }
            if(Integer.parseInt(value)<=0)
            {
                JOptionPane.showMessageDialog(null, "value must be gather than 0");
                updatestudent.setVisible(false);
            }
            else
            {
                
              updatestudent.setVisible(true);
            updatestudent.getStuUpdateBtn().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                counter++;
                UddateStudent();
                if(counter==Integer.parseInt(value))
                {
                         if(datacontrol.UpdateStudentBatch()){
                            JOptionPane.showMessageDialog(null, (counter)+" items Updated successfully");
                            updatestudent.dispose();
                            DashboardController dcontrol= new DashboardController(new Dashboard());
                            dcontrol.initDashboard();
                         }
                         else
                         {
                             JOptionPane.showMessageDialog(null, "error in update");
                         }
                         
                        
                       
                }
                
            }
        });
            updatestudent.getBackBtn().addActionListener(e -> goBack());
        
        ShowCourses();
        updatestudent.getCoueseNameUpdateList().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                itemSelectionIndex= updatestudent.getCoueseNameUpdateList().getSelectedIndex();
                switch(itemSelectionIndex)
                {
                    case 0:
                         SelectedCourse="BANGLA";
                         break;
                    case 1: 
                         SelectedCourse="ENGLISH";
                         break;
                    case 2: 
                         SelectedCourse="PHYSICS";
                         break;
                    case 3:
                         SelectedCourse="MATHEMATICS";
                         break;
                    case 4:
                         SelectedCourse="CHERMISTRY";
                         break;
                    case 5:
                         SelectedCourse="BIOLOGY";
                         break;
                         
                }
               
            }
        });
        
            }
    }
    public void goBack()
    {
        updatestudent.dispose();
        DashboardController dcontrol= new DashboardController(new Dashboard());
        dcontrol.initDashboard();
    }
    public void UddateStudent()
    { 
       int id= Integer.parseInt(updatestudent.getUpdateIDField().getText());
       String name=updatestudent.getSNmUpdateBtn().getText();
       String classs= updatestudent.getSClaUpdateBtn().getText();
       
      
       datacontrol.UpdateQueriesStudent(id, name, classs,SelectedCourse);
       
       JOptionPane.showMessageDialog(null, "item added in stack"); 
       resets();
    }
    
    public void ShowCourses(){
        for(CourseDescription.CourseName cname:CourseDescription.CourseName.values() )
            {
                updatestudent.getCoueseNameUpdateList().addItem(cname.name());
            }
       
    }
    
    public void resets(){
       updatestudent.getSNmUpdateBtn().setText("");
       updatestudent.getSClaUpdateBtn().setText("");
       updatestudent.getSeletedcourseField().setText("");
    }
    
}
