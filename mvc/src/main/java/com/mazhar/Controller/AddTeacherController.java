/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.Controller;

import com.mazhar.DataBase.DataControl;
import com.mazhar.Model.CourseDescription;
import com.mazhar.View.AddTeacher;
import com.mazhar.View.Dashboard;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author pk
 */
public class AddTeacherController {
    private AddTeacher addteacher;
    private DataControl datacontrol= new DataControl();
    public int itemSelectionIndex=0;
    public AddTeacherController(AddTeacher addteacher) {
        this.addteacher = addteacher;
    }
    
    public void initTeacher(){
        addteacher.setVisible(true);
        
        ShowCourses();
        addteacher.getTeacherCourseName().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               
                itemSelectionIndex= addteacher.getTeacherCourseName().getSelectedIndex();
                
               
                
            }
        });
        addteacher.getBackBtn().addActionListener(e-> goBack());
    }
    public void goBack(){
        addteacher.dispose();
        DashboardController dcontrol= new DashboardController(new Dashboard());
        dcontrol.initDashboard();
    }
   public void addTecher(){
       String id= addteacher.getTecIdField().getText();
       String Name= addteacher.getTecNameField().getText();
       String Course= addteacher.getTeacherCourseName().getItemAt(itemSelectionIndex);
       
       if(datacontrol.addTeacher(id, Name, Course))
       {
           JOptionPane.showMessageDialog(null, "success");
       }
       else
       {
           JOptionPane.showMessageDialog(null, "fail");
       }
   } 
    public void ShowCourses(){
        for(CourseDescription.CourseName cname:CourseDescription.CourseName.values() )
            {
                addteacher.getTeacherCourseName().addItem(cname.name());
            }
       
    }
}
