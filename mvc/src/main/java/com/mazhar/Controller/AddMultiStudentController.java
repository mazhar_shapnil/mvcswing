/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mazhar.Controller;

import com.mazhar.Model.CourseDescription;
import com.mazhar.View.AddMultiStudent;
import com.mazhar.View.Dashboard;
import com.mazhar.DataBase.DataControl;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author pk
 */
public class AddMultiStudentController {
    private AddMultiStudent multistudent;
    private  String value= "";
    private int itemSelectionIndex=0;
    private int counter=0;
    private DataControl datacontrol = new  DataControl();
    public AddMultiStudentController(AddMultiStudent multistudent) {
        this.multistudent = multistudent;
        
    }

    public void initMulti(){
        value = JOptionPane.showInputDialog(null, "How Many Student do you want to add for a course?");
        //multistudent.setVisible(true);
        if(value.equals(""))
        {
            value= "-1";
        }
       
        if(Integer.parseInt(value)<=0)
         {
              JOptionPane.showMessageDialog(null, "value must be gather than 0");
               multistudent.setVisible(false);
         }
         else
         {
              multistudent.setVisible(true);
        
        multistudent.OfferedCourses.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data="";
                itemSelectionIndex= multistudent.OfferedCourses.getSelectedIndex();
                switch(itemSelectionIndex)
                {
                    case 0:
                         data=("BANGLA");
                         break;
                    case 1: 
                         data=("ENGLISH");
                         break;
                    case 2: 
                         data=("PHYSICS");
                         break;
                    case 3:
                         data=("MATHEMATICS");
                         break;
                    case 4:
                         data=("CHERMISTRY");
                         break;
                    case 5:
                         data=("BIOLOGY");
                         break;
                         
                }
                
                multistudent.seletedcourse.setText(data);
            }
        });
        
        multistudent.getAddmultistudentBtn().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               
                counter++;
              saveMultiStudent();
               if(counter==1)
               {
                   multistudent.OfferedCourses.setVisible(false);
                   
               }
                if(counter==Integer.parseInt(value))
                {
                     
                         if(datacontrol.ExecuteQueryStudentBatch()){
                            JOptionPane.showMessageDialog(null, (counter)+" items added successfully");
                            multistudent.dispose();
                            DashboardController dcontrol= new DashboardController(new Dashboard());
                            dcontrol.initDashboard();
                         }
                         else
                         {
                             JOptionPane.showMessageDialog(null, "error in adding");
                         }
                         
                        
                       
                }
               
            }
        });
         
         }
        
       
        
        
    }
    public void ShowCourses(){
        for(CourseDescription.CourseName cname:CourseDescription.CourseName.values() )
            {
                multistudent.OfferedCourses.addItem(cname.name());
            }
       
    }
    
    public void saveMultiStudent()
    { 
       String SelectedSub=multistudent.seletedcourse.getText();
       String id=multistudent.stuId.getText() ;
       String name=multistudent.stuname.getText();
       String classs= multistudent.stuclass.getText();
       datacontrol.addQueriesStudent(id, name, classs,SelectedSub);
       JOptionPane.showMessageDialog(null, "counter="+SelectedSub+"  loop range:"+name); 
       resets();
    }
    
    public void resets()
    {
        multistudent.stuId.setText("");
        multistudent.stuname.setText("");
        multistudent.stuclass.setText("");
    }
}
